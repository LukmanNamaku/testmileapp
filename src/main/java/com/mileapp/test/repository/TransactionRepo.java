package com.mileapp.test.repository;

import com.mileapp.test.dto.TransactionRequestDto;
import com.mileapp.test.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface TransactionRepo extends JpaRepository<Transaction, Long> {

    Transaction getById(Long id);
//    Transaction findTopByTransactionIdAndTraAndTransactionCode(String trxId, String trxCode);
}
