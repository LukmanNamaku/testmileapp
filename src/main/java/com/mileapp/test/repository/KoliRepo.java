package com.mileapp.test.repository;

import com.mileapp.test.entity.Koli;
import com.mileapp.test.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface KoliRepo extends JpaRepository<Koli, Long> {

    List<Koli> findByConnoteId(String connoteId);

}
