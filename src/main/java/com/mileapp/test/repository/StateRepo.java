package com.mileapp.test.repository;

import com.mileapp.test.entity.State;
import com.mileapp.test.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface StateRepo extends JpaRepository<State, Long> {

    State getById(Long StateId);

    State getById(State transactionState);
}
