package com.mileapp.test.repository;

import com.mileapp.test.dto.LocationDto;
import com.mileapp.test.entity.Location;
import com.mileapp.test.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface LocationRepo extends JpaRepository<Location, Long> {

    Location getById(Long locationId);

    LocationDto findTopById(Long locationId);

}
