package com.mileapp.test.repository;

import com.mileapp.test.dto.CustomerResponseDto;
import com.mileapp.test.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface CustomerRepo extends JpaRepository<Customer, Long> {

    Customer getById(Long customerId);

    CustomerResponseDto getTopById(Long customerId);
}
