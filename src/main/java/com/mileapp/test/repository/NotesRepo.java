package com.mileapp.test.repository;

import com.mileapp.test.dto.NotesDto;
import com.mileapp.test.entity.Notes;
import com.mileapp.test.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface NotesRepo extends JpaRepository<Notes, Long> {

    Notes getTopById(Long notesId);
    Notes findTopByTransactionId(String transactionId);
    Notes findTopByIdAndTransactionId(Long notesId, String transactionId);
    Notes deleteByIdAndTransactionId(Long notesId, String transactionId);

    NotesDto findTopById(Long notesId);

}
