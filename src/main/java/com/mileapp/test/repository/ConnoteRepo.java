package com.mileapp.test.repository;

import com.mileapp.test.dto.ConnoteDto;
import com.mileapp.test.entity.Connote;
import com.mileapp.test.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface ConnoteRepo extends JpaRepository<Connote, Long> {

    Connote getById(Long connoteId);

    ConnoteDto findTopById(Long connoteId);
}
