package com.mileapp.test.repository;

import com.mileapp.test.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface OrganizationRepo extends JpaRepository<Organization, Long>{

    Organization getById(Long organizationId);

}
