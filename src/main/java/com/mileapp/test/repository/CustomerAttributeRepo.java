package com.mileapp.test.repository;

import com.mileapp.test.dto.CustomerAttributeResponseDto;
import com.mileapp.test.entity.Customer;
import com.mileapp.test.entity.CustomerAttribute;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
public interface CustomerAttributeRepo extends JpaRepository<CustomerAttribute, Long> {

    CustomerAttributeResponseDto getByCustomerId(Long customerId);

}
