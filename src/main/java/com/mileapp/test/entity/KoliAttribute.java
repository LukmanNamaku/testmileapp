package com.mileapp.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mileapp.test.dto.KoliAttributeDto;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 28/09/2020
 */

@Entity
@Table(name = "m_koli_attribute")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createAt", "updatedAt"},allowGetters = true)
public class KoliAttribute extends KoliAttributeDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String awbSicepat;
    private String hargaBarang;

    @Column(nullable = false, updatable = false)
    @Temporal (TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    @Override
    public String getAwbSicepat() {
        return awbSicepat;
    }

    @Override
    public void setAwbSicepat(String awbSicepat) {
        this.awbSicepat = awbSicepat;
    }

    @Override
    public String getHargaBarang() {
        return hargaBarang;
    }

    @Override
    public void setHargaBarang(String hargaBarang) {
        this.hargaBarang = hargaBarang;
    }
}
