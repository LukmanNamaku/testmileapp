package com.mileapp.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mileapp.test.dto.ConnoteDto;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 28/09/2020
 */

@Entity
@Table(name = "m_connote")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createAt", "updateAt"},allowGetters = true)
public class Connote extends ConnoteDto{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String connoteId;

    @NotNull
    private String connoteCode;

    private Integer connoterNumber;

    @NotNull
    private String connoteService;
    private String connoteServicePrice;
    private String connoteAmount;

    private String connoteBookingCode;
    private String connoteOrder;
    private String surchargeAmount;

    private Integer actualWeight;
    private Integer volumeWeight;
    private Integer chargeableWeight;

    private String transactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    private State connoteState;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Zone zoneCodeFrom;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Zone zoneCodeTo;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Organization organization;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Location location;

    private String connoteTotalPackage;
    private String connoteSurchargeAmount;
    private String connoteSlaDay;

    private String sourceTariffDb;
    private String idSourceTariff;

    private String pod;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<History> history;

    @Column(nullable = false, updatable = false)
    @Temporal (TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConnoteId() {
        return connoteId;
    }

    public void setConnoteId(String connoteId) {
        this.connoteId = connoteId;
    }

    public String getConnoteCode() {
        return connoteCode;
    }

    public void setConnoteCode(String connoteCode) {
        this.connoteCode = connoteCode;
    }

    public String getConnoteService() {
        return connoteService;
    }

    public void setConnoteService(String connoteService) {
        this.connoteService = connoteService;
    }

    public String getConnoteServicePrice() {
        return connoteServicePrice;
    }

    public void setConnoteServicePrice(String connoteServicePrice) {
        this.connoteServicePrice = connoteServicePrice;
    }

    public String getConnoteAmount() {
        return connoteAmount;
    }

    public void setConnoteAmount(String connoteAmount) {
        this.connoteAmount = connoteAmount;
    }

    public String getConnoteBookingCode() {
        return connoteBookingCode;
    }

    public void setConnoteBookingCode(String connoteBookingCode) {
        this.connoteBookingCode = connoteBookingCode;
    }

    public String getConnoteOrder() {
        return connoteOrder;
    }

    public void setConnoteOrder(String connoteOrder) {
        this.connoteOrder = connoteOrder;
    }

    public String getSurchargeAmount() {
        return surchargeAmount;
    }

    public void setSurchargeAmount(String surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }

    public Integer getActualWeight() {
        return actualWeight;
    }

    public void setActualWeight(Integer actualWeight) {
        this.actualWeight = actualWeight;
    }

    public Integer getVolumeWeight() {
        return volumeWeight;
    }

    public void setVolumeWeight(Integer volumeWeight) {
        this.volumeWeight = volumeWeight;
    }

    public Integer getChargeableWeight() {
        return chargeableWeight;
    }

    public void setChargeableWeight(Integer chargeableWeight) {
        this.chargeableWeight = chargeableWeight;
    }

    public State getConnoteState() {
        return connoteState;
    }

    public void setConnoteState(State connoteState) {
        this.connoteState = connoteState;
    }

    public Zone getZoneCodeFrom() {
        return zoneCodeFrom;
    }

    public void setZoneCodeFrom(Zone zoneCodeFrom) {
        this.zoneCodeFrom = zoneCodeFrom;
    }

    public Zone getZoneCodeTo() {
        return zoneCodeTo;
    }

    public void setZoneCodeTo(Zone zoneCodeTo) {
        this.zoneCodeTo = zoneCodeTo;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getConnoteTotalPackage() {
        return connoteTotalPackage;
    }

    public void setConnoteTotalPackage(String connoteTotalPackage) {
        this.connoteTotalPackage = connoteTotalPackage;
    }

    public String getConnoteSurchargeAmount() {
        return connoteSurchargeAmount;
    }

    public void setConnoteSurchargeAmount(String connoteSurchargeAmount) {
        this.connoteSurchargeAmount = connoteSurchargeAmount;
    }

    public String getConnoteSlaDay() {
        return connoteSlaDay;
    }

    public void setConnoteSlaDay(String connoteSlaDay) {
        this.connoteSlaDay = connoteSlaDay;
    }

    public String getSourceTariffDb() {
        return sourceTariffDb;
    }

    public void setSourceTariffDb(String sourceTariffDb) {
        this.sourceTariffDb = sourceTariffDb;
    }

    public String getIdSourceTariff() {
        return idSourceTariff;
    }

    public void setIdSourceTariff(String idSourceTariff) {
        this.idSourceTariff = idSourceTariff;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }
}
