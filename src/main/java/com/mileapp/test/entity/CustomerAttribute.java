package com.mileapp.test.entity;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 28/09/2020
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mileapp.test.dto.CustomerAttributeResponseDto;
import com.mileapp.test.dto.CustomerResponseDto;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "m_customer_attribute")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createAt", "updateAt"},allowGetters = true)

public class CustomerAttribute extends CustomerAttributeResponseDto{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String salesName;
    private String top;
    private String jenisPelanggan;

    @ManyToOne(fetch = FetchType.EAGER)
    private Customer customer;

    @Override
    public String getSalesName() {
        return salesName;
    }

    @Override
    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    @Override
    public String getTop() {
        return top;
    }

    @Override
    public void setTop(String top) {
        this.top = top;
    }

    @Override
    public String getJenisPelanggan() {
        return jenisPelanggan;
    }

    @Override
    public void setJenisPelanggan(String jenisPelanggan) {
        this.jenisPelanggan = jenisPelanggan;
    }
}
