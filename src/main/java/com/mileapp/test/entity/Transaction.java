package com.mileapp.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mileapp.test.dto.TransactionResponseDto;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 28/09/2020
 */

@Entity
@Table(name = "trx_data")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createAt", "updateAt"},allowGetters = true)
public class Transaction{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //Prever UUID
    @NotNull
    private String transactionId;

    private String customerName;
    private String customerCode;

    private String transactionAmount;
    private String transactionDiscount;
    private String transactionAdditionalField;
    private String transactionPaymentType;
    private String transactionCode;
    private Integer transactionOrder;
    private String transactionPaymentTypeName;
    private Integer transactionCashAmount;
    private Integer transactionCashChange;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Customer originData;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Customer destinationData;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private State transactionState;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Organization organization;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Customer customer;

    @ManyToOne(fetch = FetchType.EAGER)
    private Connote connote;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Location location;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Notes notes;

    @Column(nullable = false, updatable = false)
    @Temporal (TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionDiscount() {
        return transactionDiscount;
    }

    public void setTransactionDiscount(String transactionDiscount) {
        this.transactionDiscount = transactionDiscount;
    }

    public String getTransactionAdditionalField() {
        return transactionAdditionalField;
    }

    public void setTransactionAdditionalField(String transactionAdditionalField) {
        this.transactionAdditionalField = transactionAdditionalField;
    }

    public String getTransactionPaymentType() {
        return transactionPaymentType;
    }

    public void setTransactionPaymentType(String transactionPaymentType) {
        this.transactionPaymentType = transactionPaymentType;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public Integer getTransactionOrder() {
        return transactionOrder;
    }

    public void setTransactionOrder(Integer transactionOrder) {
        this.transactionOrder = transactionOrder;
    }

    public String getTransactionPaymentTypeName() {
        return transactionPaymentTypeName;
    }

    public void setTransactionPaymentTypeName(String transactionPaymentTypeName) {
        this.transactionPaymentTypeName = transactionPaymentTypeName;
    }

    public Integer getTransactionCashAmount() {
        return transactionCashAmount;
    }

    public void setTransactionCashAmount(Integer transactionCashAmount) {
        this.transactionCashAmount = transactionCashAmount;
    }

    public Integer getTransactionCashChange() {
        return transactionCashChange;
    }

    public void setTransactionCashChange(Integer transactionCashChange) {
        this.transactionCashChange = transactionCashChange;
    }

    public Customer getOriginData() {
        return originData;
    }

    public void setOriginData(Customer originData) {
        this.originData = originData;
    }

    public Customer getDestinationData() {
        return destinationData;
    }

    public void setDestinationData(Customer destinationData) {
        this.destinationData = destinationData;
    }

    public State getTransactionState() {
        return transactionState;
    }

    public void setTransactionState(State transactionState) {
        this.transactionState = transactionState;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Connote getConnote() {
        return connote;
    }

    public void setConnote(Connote connote) {
        this.connote = connote;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Notes getNotes() {
        return notes;
    }

    public void setNotes(Notes notes) {
        this.notes = notes;
    }
}
