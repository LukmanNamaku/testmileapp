package com.mileapp.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 28/09/2020
 */

@Entity
@Table(name = "m_koli")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createAt", "updatedAt"},allowGetters = true)
public class Koli {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String koliId;

    @NotNull
    private String koliCode;

    private String awbUrl;
    private Integer koliLength;
    private Integer koliChargeableWeight;
    private Integer koliHeight;
    private Integer koliVolume;
    private Integer koliWeight;

    private String koliDescription;
    private String koliFormulaId;

    private String connoteId;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private KoliAttribute koliAttribute;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<KoliSurcharge> koliSurcharge;

    @Column(nullable = false, updatable = false)
    @Temporal (TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public String getKoliId() {
        return koliId;
    }

    public void setKoliId(String koliId) {
        this.koliId = koliId;
    }

    public String getKoliCode() {
        return koliCode;
    }

    public void setKoliCode(String koliCode) {
        this.koliCode = koliCode;
    }

    public String getAwbUrl() {
        return awbUrl;
    }

    public void setAwbUrl(String awbUrl) {
        this.awbUrl = awbUrl;
    }

    public Integer getKoliLength() {
        return koliLength;
    }

    public void setKoliLength(Integer koliLength) {
        this.koliLength = koliLength;
    }

    public Integer getKoliChargeableWeight() {
        return koliChargeableWeight;
    }

    public void setKoliChargeableWeight(Integer koliChargeableWeight) {
        this.koliChargeableWeight = koliChargeableWeight;
    }

    public Integer getKoliHeight() {
        return koliHeight;
    }

    public void setKoliHeight(Integer koliHeight) {
        this.koliHeight = koliHeight;
    }

    public Integer getKoliVolume() {
        return koliVolume;
    }

    public void setKoliVolume(Integer koliVolume) {
        this.koliVolume = koliVolume;
    }

    public Integer getKoliWeight() {
        return koliWeight;
    }

    public void setKoliWeight(Integer koliWeight) {
        this.koliWeight = koliWeight;
    }

    public String getKoliDescription() {
        return koliDescription;
    }

    public void setKoliDescription(String koliDescription) {
        this.koliDescription = koliDescription;
    }

    public String getKoliFormulaId() {
        return koliFormulaId;
    }

    public void setKoliFormulaId(String koliFormulaId) {
        this.koliFormulaId = koliFormulaId;
    }

    public String getConnoteId() {
        return connoteId;
    }

    public void setConnoteId(String connoteId) {
        this.connoteId = connoteId;
    }

    public KoliAttribute getKoliAttribute() {
        return koliAttribute;
    }

    public void setKoliAttribute(KoliAttribute koliAttribute) {
        this.koliAttribute = koliAttribute;
    }

    public List<KoliSurcharge> getKoliSurcharge() {
        return koliSurcharge;
    }

    public void setKoliSurcharge(List<KoliSurcharge> koliSurcharge) {
        this.koliSurcharge = koliSurcharge;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
