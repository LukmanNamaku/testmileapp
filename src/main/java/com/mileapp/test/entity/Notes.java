package com.mileapp.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mileapp.test.dto.NotesDto;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 28/09/2020
 */
@Entity
@Table(name = "m_notes")
@EntityListeners(AuditingEntityListener.class)
public class Notes extends NotesDto{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String transactionId;

    private String catatanTambahan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCatatanTambahan() {
        return catatanTambahan;
    }

    public void setCatatanTambahan(String catatanTambahan) {
        this.catatanTambahan = catatanTambahan;
    }
}
