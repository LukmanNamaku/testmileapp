package com.mileapp.test.dto;

import lombok.Data;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */

@Data
public class TransactionUpdateDto {

    private String customerName;
    private String customerCode;
    private String transactionAmount;
    private String transactionDiscount;
    private String transactionAdditionalField;

    private String transactionPaymentType;
    private String transactionPaymentTypeName;

    private Integer transactionOrder;
    private Integer transactionCashAmount;
    private Integer transactionCashChange;

    private Integer originData;
    private Integer destinationData;
    private Integer transactionState;
    private Integer organization;
    private Integer customer;
    private Integer connote;
    private Integer location;
    private String notes;
}
