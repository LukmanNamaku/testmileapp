package com.mileapp.test.dto;

import lombok.Data;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
@Data
public class StateDto {

    private String stateCode;

}
