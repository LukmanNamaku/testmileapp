package com.mileapp.test.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 28/09/2020
 */

@Data
public class KoliAttributeDto {

    private String awbSicepat;
    private String hargaBarang;
}
