package com.mileapp.test.dto;

import com.mileapp.test.entity.History;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
@Data
public class NotesDto {

    private String catatanTambahan;

}
