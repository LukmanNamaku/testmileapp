package com.mileapp.test.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mileapp.test.entity.Customer;
import com.mileapp.test.entity.Organization;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
@Data
public class CustomerAttributeResponseDto {

    private String salesName;
    private String top;
    private String jenisPelanggan;

}
