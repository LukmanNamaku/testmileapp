package com.mileapp.test.dto;

import com.mileapp.test.entity.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
@Data
public class ConnoteDto {

    private String connoteId;
    private String connoteCode;
    private String transactionId;
    private Integer connoteNumber;

    private String connoteService;
    private String connoteServicePrice;
    private String connoteAmount;
    private String connoteBookingCode;
    private String connoteOrder;
    private String surchargeAmount;
    private Integer actualWeight;
    private Integer volumeWeight;
    private Integer chargeableWeight;

    private StateDto connoteState;

    private ZoneDto zoneCodeFrom;
    private ZoneDto zoneCodeTo;

    private Integer organizationId;
//    private Organization organization;

    private String locationId;
    private String locationName;
    private String locationType;
//    private Location location;

    private String connoteTotalPackage;
    private String connoteSurchargeAmount;
    private String connoteSlaDay;
    private String sourceTariffDb;
    private String idSourceTariff;
    private String pod;
    private List<History> history;
    private Date createdAt;
    private Date updatedAt;

}
