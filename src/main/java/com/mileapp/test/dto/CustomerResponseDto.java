package com.mileapp.test.dto;

import com.mileapp.test.entity.Organization;
import com.mileapp.test.entity.Zone;
import lombok.Data;

import java.util.Date;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */
@Data
public class CustomerResponseDto {

    private String customerName;
    private String customerAddress;
    private String customerEmail;
    private String customerPhone;
    private String customerAddressDetail;
    private String customerZipCode;
    private Zone zone;
    private Organization organization;

    private Date createdAt;
    private Date updatedAt;
}
