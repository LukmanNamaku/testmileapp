package com.mileapp.test.dto;

import com.mileapp.test.entity.*;
import lombok.Data;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */

@Data
public class TransactionResponseDto {

    private Long id;
    private String transactionId;
    private String transactionCode;

    private String customerName;
    private String customerCode;
    private String transactionAmount;
    private String transactionDiscount;
    private String transactionAdditionalField;

    //Sementara !
    private String transactionPaymentType;
    private String transactionPaymentTypeName;

    private Integer transactionOrder;
    private Integer transactionCashAmount;
    private Integer transactionCashChange;

    private String transactionState;
    private String connoteId;

    private CustomerResponseDto originData;
    private CustomerResponseDto destinationData;

    private Organization organization;
    private CustomerAttributeResponseDto customerAttribute;
    private ConnoteDto connote;
    private LocationDto currentLocation;
    private NotesDto customFields;

    private List<Koli> koliData;

    private Date createdAt;
    private Date updatedAt;

}
