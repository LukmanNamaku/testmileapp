package com.mileapp.test.controller;

import com.mileapp.test.dto.*;
import com.mileapp.test.entity.*;
import com.mileapp.test.repository.*;
import javassist.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.mappers.ModelMapper;

import javax.annotation.Resource;
import javax.swing.text.DateFormatter;
import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author Lukman Ardi - lukman.ardie@gmail.com
 * 29/09/2020
 */

/**
    1. GET /package
    2. GET /package/{id}
    3. POST /package
    4. PUT /package/{id}
    5. PATCH /package/{id}
    6. DELETE /package/{id}
*/

@RestController
@RequestMapping("/package")
public class PackageController {

    @Autowired
    TransactionRepo transactionRepo;

    @Autowired
    OrganizationRepo organizationRepo;

    @Autowired
    NotesRepo notesRepo;

    @Autowired
    LocationRepo locationRepo;

    @Autowired
    ConnoteRepo connoteRepo;

    @Autowired
    CustomerRepo customerRepo;

    @Autowired
    StateRepo stateRepo;

    @Autowired
    CustomerAttributeRepo customerAttributeRepo;

    @Autowired
    KoliRepo koliRepo;

    @GetMapping("/")
    public List<Transaction> getAllTransactions(){
        return transactionRepo.findAll();
    }

    @GetMapping("/{idTransaction}")
    public TransactionResponseDto getDetailTransaction(@PathVariable (value="idTransaction") Long idTransaction){

        Transaction transaction = transactionRepo.getById(idTransaction);

        CustomerResponseDto originData = customerRepo.getById(transaction.getOriginData().getId());
        CustomerResponseDto destinationData = customerRepo.getById(transaction.getDestinationData().getId());

        CustomerAttributeResponseDto customerAttribute = customerAttributeRepo.getByCustomerId(transaction.getCustomer().getId());
        ConnoteDto connoteData = connoteRepo.findTopById(transaction.getConnote().getId());

        LocationDto currentLocation = locationRepo.findTopById(transaction.getLocation().getId());
        NotesDto notesData = notesRepo.findTopById(transaction.getNotes().getId());

        List<Koli> koliList = koliRepo.findByConnoteId(transaction.getConnote().getConnoteId());

        TransactionResponseDto response = new TransactionResponseDto();
        response.setId(transaction.getId());
        response.setTransactionId(transaction.getTransactionId());
        response.setTransactionCode(transaction.getTransactionCode());

        response.setCustomerCode(transaction.getCustomerCode());
        response.setCustomerName(transaction.getCustomerName());

        response.setTransactionAmount(transaction.getTransactionAmount());
        response.setTransactionDiscount(transaction.getTransactionDiscount());
        response.setTransactionAdditionalField(transaction.getTransactionAdditionalField());
        response.setTransactionPaymentType(transaction.getTransactionPaymentType());
        response.setTransactionPaymentTypeName(transaction.getTransactionPaymentTypeName());

        response.setCreatedAt(transaction.getCreatedAt());
        response.setUpdatedAt(transaction.getUpdatedAt());

        response.setTransactionCashAmount(transaction.getTransactionCashAmount());
        response.setTransactionCashChange(transaction.getTransactionCashChange());

        response.setOriginData(originData);
        response.setDestinationData(destinationData);

        response.setCustomerAttribute(customerAttribute);

        response.setConnote(connoteData);
        response.setConnoteId(connoteData.getConnoteId());

        response.setCustomFields(notesData);
        response.setCurrentLocation(currentLocation);

        response.setKoliData(koliList);
        return response;
    }

    @PostMapping("/")
    public ResponseEntity addTransaction(@Valid @RequestBody TransactionRequestDto transactionRequestDto) {

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");

        //Example Code CGKFT20200715121 (Code+Date+transactionOrder)
        String transactionCode = "CGKFT" + formatter.format(now) + transactionRequestDto.getTransactionOrder();

        //Example Code d0090c40-539f-479a-8274-899b9970bddc
        String trxId = UUID.randomUUID().toString();

        Organization organization = organizationRepo.getById((long)transactionRequestDto.getOrganization());
        Location location = locationRepo.getById((long)transactionRequestDto.getLocation());
        Connote connote = connoteRepo.getById((long)transactionRequestDto.getConnote());
        Customer originData = customerRepo.getById((long)transactionRequestDto.getOriginData());
        Customer destinationData = customerRepo.getById((long)transactionRequestDto.getDestinationData());
        Customer customer = customerRepo.getById((long)transactionRequestDto.getCustomer());
        State state = stateRepo.getById((long)transactionRequestDto.getTransactionState());

        Notes newNotes = null;
        Transaction newTransaction = new Transaction();
        BeanUtils.copyProperties(transactionRequestDto, newTransaction);

        newTransaction.setTransactionId(trxId);
        newTransaction.setTransactionCode(transactionCode);
        newTransaction.setOrganization(organization);
        newTransaction.setLocation(location);
        newTransaction.setOriginData(originData);
        newTransaction.setDestinationData(destinationData);
        newTransaction.setConnote(connote);
        newTransaction.setCustomer(customer);
        newTransaction.setTransactionState(state);

        newNotes = new Notes();
        newNotes.setTransactionId(trxId);
        newNotes.setCatatanTambahan(transactionRequestDto.getNotes());

        if (!transactionRequestDto.getNotes().equals("")){
            notesRepo.save(newNotes);
        }

        newTransaction.setNotes(newNotes);
        transactionRepo.save(newTransaction);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/examplePut/{idTransaction}")
    public ResponseEntity<Transaction> updateTransactionPut(@PathVariable(value="idTransaction")Long idTransaction, @RequestBody TransactionUpdateDto transactionUpdateDto){

        Transaction transaction = transactionRepo.getById(idTransaction);

        if (transaction == null) {
            return ResponseEntity.notFound().build();
        }

        Organization organization = organizationRepo.getById((long)transactionUpdateDto.getOrganization());
        Location location = locationRepo.getById((long)transactionUpdateDto.getLocation());
        Connote connote = connoteRepo.getById((long)transactionUpdateDto.getConnote());
        Customer originData = customerRepo.getById((long)transactionUpdateDto.getOriginData());
        Customer destinationData = customerRepo.getById((long)transactionUpdateDto.getDestinationData());
        Customer customer = customerRepo.getById((long)transactionUpdateDto.getCustomer());
        State state = stateRepo.getById((long)transactionUpdateDto.getTransactionState());

        Notes notes = notesRepo.getTopById(transaction.getNotes().getId());

        BeanUtils.copyProperties(transactionUpdateDto, transaction);

        transaction.setTransactionId(transaction.getTransactionId());
        transaction.setTransactionCode(transaction.getTransactionCode());
        transaction.setOrganization(organization);
        transaction.setLocation(location);
        transaction.setOriginData(originData);
        transaction.setDestinationData(destinationData);
        transaction.setConnote(connote);
        transaction.setCustomer(customer);
        transaction.setTransactionState(state);

        notes.setTransactionId(transaction.getTransactionId());
        if (notes != null){
            String notesOld = notes.getCatatanTambahan();
            String notesNew = transactionUpdateDto.getNotes();
            if (!notesNew.equals("") && !notesNew.equals(notesOld)){
                notes.setCatatanTambahan(notesNew);
                notesRepo.save(notes);
            }
        }
        transactionRepo.save(transaction);
        return ResponseEntity.ok(transaction);

    }

    @PatchMapping("/examplePatch/{idTransaction}")
    public ResponseEntity<?> updateTransactionPatch(@PathVariable(value="idTransaction")Long idTransaction, @Valid @RequestBody TransactionUpdateDto transactionUpdateDto){

        Transaction transaction = transactionRepo.getById(idTransaction);
        if (transaction == null) {
            return ResponseEntity.notFound().build();
        }

        transaction.setTransactionId(transaction.getTransactionId());
        transaction.setTransactionCode(transaction.getTransactionCode());

        if (transactionUpdateDto.getCustomerName() != null){
            transaction.setCustomerName(transactionUpdateDto.getCustomerName());
        }

        if (transactionUpdateDto.getCustomerCode() != null){
            transaction.setCustomerCode(transactionUpdateDto.getCustomerCode());
        }

        if (transactionUpdateDto.getTransactionAmount() != null){
            transaction.setTransactionAmount(transactionUpdateDto.getTransactionAmount());
        }

        if (transactionUpdateDto.getTransactionDiscount() != null){
            transaction.setTransactionDiscount(transactionUpdateDto.getTransactionDiscount());
        }

        if (transactionUpdateDto.getTransactionAdditionalField() != null){
            transaction.setTransactionAdditionalField(transactionUpdateDto.getTransactionAdditionalField());
        }

        if (transactionUpdateDto.getTransactionPaymentType() != null){
            transaction.setTransactionPaymentType(transactionUpdateDto.getTransactionPaymentType());
        }

        if (transactionUpdateDto.getTransactionPaymentTypeName() != null){
            transaction.setTransactionPaymentTypeName(transactionUpdateDto.getTransactionPaymentTypeName());
        }

        if (transactionUpdateDto.getTransactionOrder() != null){
            transaction.setTransactionOrder(transactionUpdateDto.getTransactionOrder());
        }

        if (transactionUpdateDto.getTransactionCashAmount() != null){
            transaction.setTransactionCashAmount(transactionUpdateDto.getTransactionCashAmount());
        }

        if (transactionUpdateDto.getTransactionCashChange() != null){
            transaction.setTransactionCashChange(transactionUpdateDto.getTransactionCashChange());
        }

        if ( transactionUpdateDto.getOrganization() != null){
            Organization organization = organizationRepo.getById((long)transactionUpdateDto.getOrganization());
            transaction.setOrganization(organization);
        }

        if ( transactionUpdateDto.getLocation() != null){
            Location location = locationRepo.getById((long)transactionUpdateDto.getLocation());
            transaction.setLocation(location);
        }

        if ( transactionUpdateDto.getOriginData() != null){
            Customer originData = customerRepo.getById((long)transactionUpdateDto.getOriginData());
            transaction.setOriginData(originData);
        }

        if ( transactionUpdateDto.getDestinationData() != null){
            Customer destinationData = customerRepo.getById((long)transactionUpdateDto.getDestinationData());
            transaction.setDestinationData(destinationData);
        }

        if ( transactionUpdateDto.getConnote() != null){
            Connote connote = connoteRepo.getById((long)transactionUpdateDto.getConnote());
            transaction.setConnote(connote);
        }

        if ( transactionUpdateDto.getCustomer() != null){
            Customer customer = customerRepo.getById((long)transactionUpdateDto.getCustomer());
            transaction.setCustomer(customer);
        }

        if (transactionUpdateDto.getTransactionState() != null){
            State state = stateRepo.getById((long)transactionUpdateDto.getTransactionState());
            transaction.setTransactionState(state);
        }

        if (transactionUpdateDto.getNotes() != null) {
            if (transaction.getNotes() != null) {
                Notes notes = notesRepo.getTopById(transaction.getNotes().getId());
                if (notes != null) {
                    if (notes.getCatatanTambahan().equals("")) {
                        String notesNew = transactionUpdateDto.getNotes();
                        notes.setCatatanTambahan(notesNew);
                    } else {
                        String notesOld = notes.getCatatanTambahan();
                        String notesNew = transactionUpdateDto.getNotes();
                        if (!notesNew.equals("") && !notesNew.equals(notesOld)) {
                            notes.setCatatanTambahan(notesNew);
                        }
                    }
                }
                notesRepo.save(notes);
            }
        }

        Transaction updateTransaction = transactionRepo.save(transaction);
        return ResponseEntity.ok(updateTransaction);
    }

    @DeleteMapping("/{idTransaction}")
    @Transactional
    public String deleteTransaction(@PathVariable (value="idTransaction") Long idTransaction){

        Transaction transaction = transactionRepo.getById(idTransaction);
        String result = "";
        if(transaction == null) {
            result = "Transaction ID : " + idTransaction + " tidak ditemukan";
            return result;
        }

        Notes notes = notesRepo.findTopByIdAndTransactionId(transaction.getNotes().getId(), transaction.getTransactionId());
        if(notes != null) {
            notesRepo.delete(notes);
        }

        transactionRepo.delete(transaction);

        result = "transaction dengan transaksi : " +transaction.getTransactionId()+ " dan code : "+ transaction.getTransactionCode()+" Berhasil dihapus !";
        return result;
    }

}
